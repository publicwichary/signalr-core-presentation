﻿# Scenario 2

- Open properties of Solution
- Select multiple statup projects option
- Select `SignalrCorePresentation.Api` as Start
- Select `SignalrCorePresentation.Scenario2` as Start
- Start debugging (F5)
- Navigate to `https://localhost:5001/api/values/trigger` in order to trigger REST API controller
- Watch console output