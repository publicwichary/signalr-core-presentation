﻿using Microsoft.AspNetCore.SignalR.Client;
using System;
using System.Threading.Tasks;
using SignalrCorePresentation.Clients.Shared.Extensions;

namespace SignalrCorePresentation.Scenario2
{
    /// <summary>
    /// HubConnection is disposable, but for the ease of demo is not properly disposed (use try-finally)
    /// </summary>
    sealed class Program
    {
        static async Task Main()
        {
            var url = "https://localhost:5001/hubs/notificationhub";
            var connection = new HubConnectionBuilder().WithUrl(url).Build();
            await connection.StartConnection();
            Console.WriteLine("If you want to exit press any key ...");
            Console.ReadLine();
            await connection.DisposeAsync();
        }
    }
}
