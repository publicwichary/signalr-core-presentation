﻿using Microsoft.AspNetCore.SignalR.Client;
using System;
using System.Net.Http;
using System.Threading.Tasks;
using Newtonsoft.Json;
using SignalrCorePresentation.Clients.Shared.Extensions;

namespace SignalrCorePresentation.Scenario4
{
    /// <summary>
    /// HubConnection is disposable, but for the ease of demo is not properly disposed (use try-finally)
    /// </summary>
    sealed class Program
    {
        static async Task Main()
        {
            var response = await new HttpClient().GetAsync("https://localhost:5001/api/values/login");
            var stringTokenModel = await response.Content.ReadAsStringAsync();
            var tokenModel = JsonConvert.DeserializeObject<TokenDto>(stringTokenModel);
            var url = "https://localhost:5001/hubs/secretstorehub";
            var connection = new HubConnectionBuilder().WithUrl(url, options =>
            {
                options.AccessTokenProvider = () => Task.FromResult(tokenModel.JwtToken);
            }).Build();
            await connection.StartConnection();
            await Task.Delay(TimeSpan.FromSeconds(5));
            await connection.HelloWorld();
            await Task.Delay(TimeSpan.FromSeconds(3));
            Console.WriteLine("If you want to exit press any key ...");
            Console.ReadLine();
            await connection.DisposeAsync();
        }
    }
}
