﻿using System;

namespace SignalrCorePresentation.Scenario4
{
    public sealed class TokenDto
    {
        public string JwtToken { get; set; }
        // ReSharper disable once UnusedAutoPropertyAccessor.Local
        public DateTime Expiration { get; set; }
    }
}
