# ASP.NET Signalr Core presentation

Author: Paweł Wichary (wicharypawel@gmail.com)

Presentation: SignalRCore.pdf

## Getting Started

1. Install .NET Core SDK
2. Open using any code editor
3. Follow README.md in each scenario project eg. `SignalrCorePresentation.Scenario1/README.md`