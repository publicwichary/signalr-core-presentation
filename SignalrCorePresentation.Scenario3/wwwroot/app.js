const connection = new signalR.HubConnectionBuilder()
    .withUrl("https://localhost:5001/hubs/notificationhub")
    .configureLogging(signalR.LogLevel.Information)
    .build();
connection.on("ReceiveNotification", (notificationName) => {
    console.log(`Notification received: ${notificationName}.`);
});
connection.start().then(function () {
    console.log("Connection & Listening started.");
    //connection.invoke("MethodName").catch(err => console.error(err.toString()));
}).catch(function (err) {
    return console.error(err.toString());
});
