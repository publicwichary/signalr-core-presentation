﻿# Scenario 3

- Open properties of Solution
- Select multiple statup projects option
- Select `SignalrCorePresentation.Api` as Start
- Select `SignalrCorePresentation.Scenario3` as Start
- Start debugging (F5)
- Open development tools (F12)
- Navigate to in separate tab `https://localhost:5001/api/values/trigger` in order to trigger REST API controller
- Watch console output in development tools