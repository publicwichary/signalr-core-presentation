﻿using System;

namespace SignalrCorePresentation.Api.App_Infrastructure.Auth
{
    public sealed class TokenDto
    {
        public string JwtToken { get; set; }
        // ReSharper disable once UnusedAutoPropertyAccessor.Local
        public DateTime Expiration { get; set; }
    }
}
