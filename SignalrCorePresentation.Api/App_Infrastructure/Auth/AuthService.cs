﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using Microsoft.IdentityModel.Tokens;

namespace SignalrCorePresentation.Api.App_Infrastructure.Auth
{
    public sealed class AuthService
    {
        public TokenDto GetChatJwtToken(string userEmail, string issuer, string audience, string secret)
        {
            var applicationUserClaims = GetApplicationUserChatClaims(userEmail);
            var jwtAuthRequiredClaims = GetJwtAuthRequiredClaims(issuer, audience, DateTime.Now.AddMinutes(10));
            var claims = jwtAuthRequiredClaims.Union(applicationUserClaims);
            var symmetricSecurityKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(secret));
            var signingCredential = new SigningCredentials(symmetricSecurityKey, SecurityAlgorithms.HmacSha256);
            var jwtHeader = new JwtHeader(signingCredential);
            var jwtPayload = new JwtPayload(claims);
            var token = new JwtSecurityToken(jwtHeader, jwtPayload);
            return new TokenDto
            {
                JwtToken = new JwtSecurityTokenHandler().WriteToken(token),
                Expiration = token.ValidTo
            };
        }

        private static IEnumerable<Claim> GetApplicationUserChatClaims(string userEmail)
        {
            return new[]
            {
                new Claim(JwtRegisteredClaimNames.Email, userEmail),
            };
        }

        private static IEnumerable<Claim> GetJwtAuthRequiredClaims(string issuer, string audience, DateTime expirationDateTime)
        {
            return new[]
            {
                new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()),
                new Claim(JwtRegisteredClaimNames.Nbf, new DateTimeOffset(DateTime.Now).ToUnixTimeSeconds().ToString()),
                new Claim(JwtRegisteredClaimNames.Exp, new DateTimeOffset(expirationDateTime).ToUnixTimeSeconds().ToString()),
                new Claim(JwtRegisteredClaimNames.Iss, issuer),
                new Claim(JwtRegisteredClaimNames.Aud, audience)
            };
        }

        // ReSharper disable once UnusedMember.Local
        private static IEnumerable<Claim> GetRolesAsClaims(IEnumerable<string> roles)
        {
            const string roleType = "http://schemas.microsoft.com/ws/2008/06/identity/claims/role";
            return roles.Select(x => new Claim(roleType, x));
        }
    }
}