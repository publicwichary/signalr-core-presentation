﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SignalR;
using Microsoft.Extensions.Configuration;
using SignalrCorePresentation.Api.App_Infrastructure.Auth;
using SignalrCorePresentation.Api.Hubs;

namespace SignalrCorePresentation.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ValuesController : ControllerBase
    {
        [HttpGet]
        public ActionResult<string> Get()
        {
            return Ok("Hello world API controller");
        }

        [HttpGet("trigger")]
        public async Task<ActionResult<string>> TriggerWebSocket([FromServices] IHubContext<NotificationHub> notificationHub)
        {
            await notificationHub.Clients.All.SendAsync("ReceiveNotification", "ControllerNotification");
            return Ok("Controller triggered");
        }

        [HttpGet("login")]
        public ActionResult<TokenDto> GetJwtForSignalr([FromServices] AuthService authService, [FromServices] IConfiguration configuration)
        {
            var audience = configuration.GetValue<string>("Auth:SignalRAudience");
            var issuer = configuration.GetValue<string>("Auth:SignalRIssuer");
            var secret = configuration.GetValue<string>("Auth:SignalRJwtKey");
            var tokenModel = authService.GetChatJwtToken("wicharypawel@gmail.com", issuer, audience, secret);
            return Ok(tokenModel);
        }
    }
}
