﻿using System.Threading.Tasks;

namespace SignalrCorePresentation.Api.Hubs
{
    public interface INotificationClient
    {
        Task ReceiveNotification(string notificationName);
    }
}