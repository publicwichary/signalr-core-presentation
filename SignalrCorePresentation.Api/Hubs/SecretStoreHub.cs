﻿using Microsoft.AspNetCore.SignalR;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;

namespace SignalrCorePresentation.Api.Hubs
{
    [Authorize(AuthenticationSchemes = "SignalRJWT")]
    public class SecretStoreHub : Hub
    {
        [Authorize]
        public async Task HelloWorld()
        {
            await Clients.All.SendAsync("Hello World SignalR Core" );
        }
    }
}
