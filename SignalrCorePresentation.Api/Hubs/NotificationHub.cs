﻿using Microsoft.AspNetCore.SignalR;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;

namespace SignalrCorePresentation.Api.Hubs
{
    [AllowAnonymous]
    public class NotificationHub : Hub<INotificationClient>
    {
        public async Task HelloWorld()
        {
            await Clients.All.ReceiveNotification("Hello World SignalR Core" );
        }

        public async Task Notification(string notificationName)
        {
            await Clients.All.ReceiveNotification(notificationName);
        }
    }
}
