﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.SignalR;
using Moq;

namespace SignalrCorePresentation.Api.UnitTests.App_Infrastructure.Extensions
{
    public static class SignalRTestUserExtensions
    {
        public static async Task ConnectUser(this TestUser testUser, Hub hub, Mock<HubCallerContext> context)
        {
            context.Setup(x => x.UserIdentifier).Returns(testUser.UserGuid.ToString());
            context.Setup(x => x.ConnectionId).Returns(testUser.ConnectionId);
            await hub.OnConnectedAsync();
            context.Setup(x => x.ConnectionId).Returns((string)null);
            context.Setup(x => x.UserIdentifier).Returns((string)null);
        }

        public static void SetCurrentUser(this TestUser testUser, Mock<HubCallerContext> context)
        {
            context.Setup(x => x.UserIdentifier).Returns(testUser.UserGuid.ToString());
            context.Setup(x => x.ConnectionId).Returns(testUser.ConnectionId);
        }

        public static async Task DisconnectUser(this TestUser testUser, Hub hub, Mock<HubCallerContext> context)
        {
            context.Setup(x => x.UserIdentifier).Returns(testUser.UserGuid.ToString());
            context.Setup(x => x.ConnectionId).Returns(testUser.ConnectionId);
            await hub.OnDisconnectedAsync(null);
            context.Setup(x => x.ConnectionId).Returns((string)null);
            context.Setup(x => x.UserIdentifier).Returns((string)null);
        }
    }
}
