﻿using System;

namespace SignalrCorePresentation.Api.UnitTests.App_Infrastructure
{
    public sealed class TestUser
    {
        private TestUser()
        {
        }

        public Guid UserGuid { get; private set; }
        public string ConnectionId { get; private set; }

        public static TestUser NewZeroGuidUser() => new TestUser
        {
            UserGuid = Guid.Empty,
            ConnectionId = Guid.NewGuid().ToString().Substring(0, 8)
        };

        public static TestUser NewUser() => new TestUser
        {
            UserGuid = Guid.NewGuid(),
            ConnectionId = Guid.NewGuid().ToString().Substring(0, 8)
        };

        public static TestUser NewUserConnection(TestUser user) => new TestUser
        {
            UserGuid = user.UserGuid,
            ConnectionId = Guid.NewGuid().ToString().Substring(0, 8)
        };
    }
}