using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.SignalR;
using Moq;
using SignalrCorePresentation.Api.Hubs;
using SignalrCorePresentation.Api.UnitTests.App_Infrastructure;
using SignalrCorePresentation.Api.UnitTests.App_Infrastructure.Extensions;
using Xunit;

// ReSharper disable PrivateFieldCanBeConvertedToLocalVariable

namespace SignalrCorePresentation.Api.UnitTests
{
    public class NotificationHubUnitTests : IDisposable
    {
        private readonly NotificationHub _hub;
        private readonly Mock<IHubCallerClients<INotificationClient>> _clients;
        private readonly Mock<IGroupManager> _groups;
        private readonly Mock<HubCallerContext> _context;
        private readonly Mock<INotificationClient> _clientsProxy;

        public NotificationHubUnitTests()
        {
            _clients = new Mock<IHubCallerClients<INotificationClient>>();
            _groups = new Mock<IGroupManager>();
            _context = new Mock<HubCallerContext>();
            _clientsProxy = new Mock<INotificationClient>();
            _hub = new NotificationHub
            {
                Context = _context.Object,
                Clients = _clients.Object,
                Groups = _groups.Object
            };
        }

        [Trait("Category", "Unit")]
        [Fact(DisplayName = nameof(ConnectUserWithHelloWorld))]
        public async Task ConnectUserWithHelloWorld()
        {
            //Arrange
            _clients.Setup(x => x.All).Returns(_clientsProxy.Object);
            var testUser1 = TestUser.NewUser();
            //Act
            await testUser1.ConnectUser(_hub, _context);
            testUser1.SetCurrentUser(_context);
            await _hub.HelloWorld();
            await testUser1.DisconnectUser(_hub, _context);
            //Assert
            _clientsProxy.Verify(x => x.ReceiveNotification(It.IsAny<string>()), Times.Once);
        }

        public void Dispose()
        {
            _hub?.Dispose();
        }
    }
}
