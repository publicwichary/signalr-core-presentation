﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.SignalR.Client;

namespace SignalrCorePresentation.Clients.Shared.Extensions
{
    public static class HubConnectionExtensions
    {
        public static async Task StartConnection(this HubConnection connection)
        {
            connection.On("ReceiveNotification", (string notificationName) =>
            {
                Console.ForegroundColor = ConsoleColor.Cyan;
                Console.WriteLine($"Notification received: {notificationName}.");
                Console.ForegroundColor = ConsoleColor.White;
            });

            try
            {
                await connection.StartAsync();
                Console.WriteLine("Connection & Listening started.");
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        public static async Task HelloWorld(this HubConnection connection)
        {
            try
            {
                Console.WriteLine("HelloWorld started.");
                await connection.InvokeAsync("HelloWorld");
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        public static async Task Notification(this HubConnection connection, string notificationName)
        {
            try
            {
                Console.WriteLine("Notification started.");
                await connection.InvokeAsync("Notification", notificationName);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }
    }
}