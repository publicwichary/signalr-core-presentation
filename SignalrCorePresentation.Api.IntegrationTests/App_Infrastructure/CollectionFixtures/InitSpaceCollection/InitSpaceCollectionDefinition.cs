﻿using Xunit;

namespace SignalrCorePresentation.Api.IntegrationTests.App_Infrastructure.CollectionFixtures.InitSpaceCollection
{
    [CollectionDefinition(nameof(InitSpaceCollectionDefinition))]
    public class InitSpaceCollectionDefinition : ICollectionFixture<InitSpaceCollectionFixture>
    {
        // This class has no code, and is never created. Its purpose is simply
        // to be the place to apply [CollectionDefinition] and all the
        // ICollectionFixture<> interfaces.
    }
}
