﻿using System;
using System.Threading.Tasks;

namespace SignalrCorePresentation.Api.IntegrationTests.App_Infrastructure.CollectionFixtures.InitSpaceCollection
{
    public class InitSpaceCollectionFixture : IDisposable
    {
        public Task Initialization { get; }

        public InitSpaceCollectionFixture()
        {
            Initialization = InitializeAsync();
        }

        private async Task InitializeAsync()
        {
            //ADD ANY INITIALIZATION CODE WITH SEED OR CLEARNING RESOURCES
        }

        public void Dispose()
        {
        }
    }
}
