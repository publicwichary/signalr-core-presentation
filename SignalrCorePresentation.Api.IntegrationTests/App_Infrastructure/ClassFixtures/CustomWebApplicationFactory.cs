﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc.Testing;
using Microsoft.Extensions.Logging;
using Microsoft.AspNetCore;

namespace SignalrCorePresentation.Api.IntegrationTests.App_Infrastructure.ClassFixtures
{
    public class CustomWebApplicationFactory<TStartup>
        : WebApplicationFactory<TStartup> where TStartup : class
    {
        protected override void ConfigureWebHost(IWebHostBuilder builder)
        {
            builder.UseEnvironment("Test");
            base.ConfigureWebHost(builder);
        }

        /// <summary>
        ///     Do not use base.CreateWebHostBuilder(); unless you want to use IWebHostBuilder CreateWebHostBuilder(string[] args)
        ///     method via reflection from Program.cs
        /// </summary>
        protected override IWebHostBuilder CreateWebHostBuilder()
        {
            return WebHost.CreateDefaultBuilder()
                .UseStartup<Startup>()
                .UseDefaultServiceProvider(options => options.ValidateScopes = false)
                .ConfigureLogging(builder =>
                {
                    builder.ClearProviders();
                });
        }
    }
}
