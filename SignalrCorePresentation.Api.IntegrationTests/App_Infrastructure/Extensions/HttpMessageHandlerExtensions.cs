﻿using System;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.AspNetCore.SignalR.Client;

namespace SignalrCorePresentation.Api.IntegrationTests.App_Infrastructure.Extensions
{
    public static class HttpMessageHandlerExtensions
    {
        public static async Task<HubConnection> StartConnectionAsync(this HttpMessageHandler handler, string hubName)
        {
            var hubConnection = new HubConnectionBuilder()
                .WithUrl($"ws://localhost/hubs/{hubName.ToLowerInvariant()}", o =>
                {
                    o.HttpMessageHandlerFactory = _ => handler;
                })
                .Build();

            await hubConnection.StartAsync();
            hubConnection.Closed += async error =>
            {
                await Task.Delay(new Random().Next(0, 5) * 1000);
                await hubConnection.StartAsync();
            };
            return hubConnection;
        }
    }
}
