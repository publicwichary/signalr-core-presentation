using System;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.AspNetCore.SignalR.Client;
using SignalrCorePresentation.Api.IntegrationTests.App_Infrastructure.ClassFixtures;
using SignalrCorePresentation.Api.IntegrationTests.App_Infrastructure.CollectionFixtures.InitSpaceCollection;
using SignalrCorePresentation.Api.IntegrationTests.App_Infrastructure.Extensions;
using Xunit;

namespace SignalrCorePresentation.Api.IntegrationTests
{
    [Collection(nameof(InitSpaceCollectionDefinition))]
    public class NotificationHubTests : IClassFixture<CustomWebApplicationFactory<Startup>>
    {

        private readonly CustomWebApplicationFactory<Startup> _factory;
        private readonly InitSpaceCollectionFixture _initSpaceCollectionFixture;

        public NotificationHubTests(CustomWebApplicationFactory<Startup> factory, InitSpaceCollectionFixture initSpaceCollectionFixture)
        {
            _factory = factory;
            _initSpaceCollectionFixture = initSpaceCollectionFixture;
        }

        [Trait("Category", "Integration")]
        [Fact(DisplayName = nameof(CallHelloWorld))]
        public async Task CallHelloWorld()
        {
            // Arrange
            await _initSpaceCollectionFixture.Initialization;
            _factory.CreateClient(); // need to create a client for the server property to be available
            var server = _factory.Server;
            var connection = await server.CreateHandler().StartConnectionAsync("NotificationHub");
            
            // Act
            string notificationNameActual = null;
            connection.On<string>("ReceiveNotification", notificationName =>
            {
                notificationNameActual = notificationName;
            });
            await connection.InvokeAsync("HelloWorld");
            await connection.DisposeAsync();
            //Assert
            Assert.Equal("Hello World SignalR Core", notificationNameActual);
        }
    }
}
